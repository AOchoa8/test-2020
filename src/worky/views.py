from django.views.generic import ListView
from django.contrib.auth.models import User


class HomeView(ListView):
    context_object_name = 'user_list'
    queryset = User.objects.filter()
    template_name = "home.html"
