from django.contrib.auth.models import User
from .serializers import UserSerializer
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status


class UserView(APIView):
    """
    Create a new user.
    """
    # POST function to create a user with serializer
    def post(self, request, format=None):
        serializer = UserSerializer(data=request.data)
        # Validates user exist
        try:
            user = User.objects.get(username=request.data["username"])
        # If it does not exist, create user with validated data
        except User.DoesNotExist:
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
        # If it exists return data of the user
        else:
            serializer = UserSerializer(user)
            return Response(serializer.data)
        # Else return validation errors
        print(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class GetUser(APIView):
    """
    Get user by pk
    """
    # GET function that validates the user exists else returns 204
    def get(self, request, pk, format=None):
        # Try to get the user by pk
        try:
            user = User.objects.get(pk=pk)
        # If user does not exist return 204
        except User.DoesNotExist:
            return Response(status=status.HTTP_204_NO_CONTENT)
        # Else return user
        else:
            serializer = UserSerializer(user)
            return Response(serializer.data)


class JWTUserView(APIView):
    """
    Create a new user authenticating with JWT
    """
    # JWT authentication restriction
    permission_classes = [IsAuthenticated]

    # POST function to create a user with serializer
    def post(self, request, format=None):
        serializer = UserSerializer(data=request.data)
        # Validates user exist
        try:
            user = User.objects.get(username=request.data["username"])
        # If it does not exist, create user with validated data
        except User.DoesNotExist:
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
        # If it exists return data of the user
        else:
            serializer = UserSerializer(user)
            return Response(serializer.data)
        # Else return validation errors
        print(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
