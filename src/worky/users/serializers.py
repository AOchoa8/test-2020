from django.contrib.auth.models import User
from rest_framework import serializers
from django.contrib.auth.hashers import make_password


class UserSerializer(serializers.HyperlinkedModelSerializer):
    username = serializers.CharField(required=True, max_length=30),
    password = serializers.CharField(style={'input_type': 'password'})
    first_name = serializers.CharField(required=True, max_length=30)
    last_name = serializers.CharField(required=True, max_length=30)
    email = serializers.EmailField(required=True)

    class Meta:
        model = User
        fields = ['pk', 'username', 'password', 'email', 'first_name', 'last_name']

    def create(self, validated_data):
        """
        Create and return a new `User` instance, given the validated data.
        """
        # Hashes the password before sending it
        validated_data['password'] = make_password(validated_data['password'])
        return User.objects.create(**validated_data)
